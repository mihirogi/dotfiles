# openmp
alias clang-omp='/usr/local/opt/llvm/bin/clang -fopenmp -L/usr/local/opt/llvm/lib -Wl,-rpath,/usr/local/opt/llvm/lib'
alias clang-omp++='/usr/local/opt/llvm/bin/clang++ -fopenmp -L/usr/local/opt/llvm/lib -Wl,-rpath,/usr/local/opt/llvm/lib'


# react-native
set -x CMAKE_PREFIX_PATH /usr/local/Cellar/qt/5.11.1/lib/cmake/

# history
## history format
function history
    builtin history --show-time='%Y/%m/%d %H:%M:%S ' | sort
end
