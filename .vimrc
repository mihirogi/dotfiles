set scrolloff=5
set visualbell t_vb=
set noerrorbells
inoremap <silent> jj <ESC>
set clipboard=unnamed,autoselect

if v:version >= 700
    nnoremap <C-m> :call OpenNewTab()<CR>
    function! OpenNewTab()
        let f = expand("%:p")
        execute ":q"
        execute ":tabnew ".f
    endfunction
endif
