## 概要

Macで使ってる設定ファイルとかを管理する

* []dotfilesから各所にシンボリックリンクを貼るシェルを書く

## 管理する物

* config.fish
 + fish shellのbashrcみたいなやつ
* vimrc
 + vimの設定


## メモ
### vim

探してたctrl + m でプレビューをタブとして開き直す  
http://d.hatena.ne.jp/m1204080/20101025/1288028786

### fish

環境変数の設定  
http://fish.rubikitch.com/tutorial/

履歴のフォーマット
http://boiled-mag.hatenablog.jp/entry/2017/06/18/133640
